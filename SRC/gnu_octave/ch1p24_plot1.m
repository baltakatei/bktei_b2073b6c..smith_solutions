  ## Ref/Attrib: "Function Reference: scatter".
  ##   URL: https://octave.sourceforge.io/octave/function/scatter.html
  ## Ref/Attrib: Smith, J.M.;
  ##   "Introduction to Chemical Engineering Thermodynamics 7th Edition";
  ##   Page 19; Chapter: 1; Section "Problems";
  ##   oclc: http://www.worldcat.org/oclc/799307624
  clf;
  temp = [-18.5, -9.5, 0.2, 11.8, 23.1, 32.7, 44.4, 52.1, 63.3, 75.5];
  psat = [3.18, 5.48, 9.45, 16.9, 28.2, 41.9, 66.6, 89.5, 129.0, 187.0];
  scatter (temp, psat, "r");
  title ("Vapor pressure (kPa) vs. Temperature (°C)");
  xlabel('Temperature [°C]');
  ylabel('Pressure [kPa]');
  grid on;