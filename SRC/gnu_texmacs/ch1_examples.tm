<TeXmacs|1.99.13>

<style|<tuple|generic|libertine-font>>

<\body>
  <\doc-data|<doc-title|Chapter 1 Examples>>
    \;
  </doc-data>

  <\itemize>
    <item>Chapter example prompts are excerpts from:

    <\quote-env>
      Smith, Joseph Mauch, Hendrick C. Van Ness, and Michael M. Abbott. 2005.
      <hlink|Introduction to chemical engineering
      thermodynamics|http://www.worldcat.org/oclc/799307624>. Boston, Mass:
      McGraw-Hill.
    </quote-env>

    Prompts are reproduced for the purpose of teaching chemical engineering
    and are therefore to be considered \PFair use\Q as defined in <hlink|17
    U.S. Code Section 107|https://www.law.cornell.edu/uscode/text/17/107>.
  </itemize>

  <\itemize>
    <item>Solutions copyrighted by <hlink|Steven Baltakatei
    Sandoval|http://baltakatei.com> and licensed under a Creative Commons
    Attribution-ShareAlike 4.0 International License (<hlink|CC BY-SA
    4.0|https://creativecommons.org/licenses/by-sa/4.0/>). Copyright
    information of derivative or attributed works such as <hlink|Wikimedia
    Commons|https://commons.wikimedia.org/wiki/Main_Page> images are noted
    where applicable.
  </itemize>

  <section|Example>

  <subsection|Prompt>

  An astronaut weighs <math|730 N> in Houston, Texas, where the local
  acceleration of gravity is <math|g=9.792
  <text|m>*\<cdot\><text|s><rsup|-2>>. What are the astronaut's mass and
  weight on the moon, where <math|g=1.67 <text|m>\<cdot\><text|s><rsup|-2>>?

  <subsection|Solution>

  Plan:

  <\enumerate-numeric>
    <item>Calculate astronaut mass, <math|m>.

    <item>Calculate weight on moon, <math|W<rsub|moon>>.
  </enumerate-numeric>

  <\equation>
    F=m*a
  </equation>

  <\equation>
    W<rsub|<text|earth>>=m*\<cdot\>g<rsub|<text|earth>><label|weight equation
    earth>
  </equation>

  <\equation>
    W<rsub|<text|moon>>=m*\<cdot\>g<rsub|<text|moon>>
  </equation>

  Identify givens:

  <\enumerate>
    <item><math|W<rsub|<text|earth>>=730 <text|N>>
  </enumerate>

  From equation <reference|weight equation earth>, solve for <math|m>:

  <\equation>
    m=<frac|W<rsub|<text|earth>>|g<rsub|<text|earth>>>
  </equation>

  Plug in values for <math|W<rsub|<text|earth>>> and
  <math|g<rsub|<text|earth>>>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|m>|<cell|=>|<cell|<frac|730 <text|N>|9.792
    <frac|<text|m>|<text|s><rsup|2>>><eq-number>>>|<row|<cell|m>|<cell|=>|<cell|<around*|(|<frac|730|9.792>|)>\<cdot\><around*|(|<text|N>\<cdot\><frac|<text|s><rsup|2>|<text|m>>|)><eq-number>>>|<row|<cell|m>|<cell|=>|<cell|<around*|(|<frac|730|9.792>|)>\<cdot\><around*|(|<around*|[|<text|kg>\<cdot\><frac|<text|m>|<text|s><rsup|2>>|]>\<cdot\><around*|[|<frac|<text|s><rsup|2>|<text|m>>|]>|)><eq-number>>>|<row|<cell|m>|<cell|=>|<cell|<around*|(|<frac|730|9.792>|)>\<cdot\><around*|(|<text|kg>|)><eq-number>>>|<row|<cell|m>|<cell|=>|<cell|74.55
    <text|kg><eq-number>>>|<row|<cell|m>|<cell|=>|<cell|74.6
    <text|kg><eq-number>>>>>
  </eqnarray*>

  The mass of the astronaut is <math|74.6 <text|kg>>. Now, using the equation
  <math|W<rsub|<text|moon>>=m*\<cdot\>g<rsub|<text|moon>>>, it is possible to
  calculate <math|W<rsub|<text|moon>>>.

  <\eqnarray*>
    <tformat|<table|<row|<cell|W<rsub|<text|moon>>>|<cell|=>|<cell|m\<cdot\>g<rsub|<text|moon>><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|74.55
    <text|kg>|)>\<cdot\><around*|(|1.67 <frac|<text|m>|<text|s><rsup|2>>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|124
    <text|kg>\<cdot\><frac|<text|m>|<text|s><rsup|2>><eq-number>>>|<row|<cell|W<rsub|<text|moon>>>|<cell|=>|<cell|124
    <text|N><eq-number>>>>>
  </eqnarray*>

  The mass of the astronaut on the moon is the same as the mass of the
  astronaut on Earth.

  <\equation>
    m<rsub|moon<text|>>=m<rsub|earth<text|>>=m=74.6 <text|kg>
  </equation>

  <section|Example>

  <subsection|Prompt>

  A dead-weight gauge with a 1-cm-diameter piston is used to measure
  pressures very accurately. In a particular instance a mass of <math|6.14
  <text|kg>> (including piston and pan) brings it into balance. If the local
  acceleration of gravity is <math|9.82 <text|m>\<cdot\><text|s><rsup|-2>>,
  what is the <em|gauge> pressure being measured? If the barometric pressure
  is <math|748<text|(torr)>>, what is the <em|absolute> pressure?

  <subsection|Solution>

  List givens:

  <\eqnarray*>
    <tformat|<table|<row|<cell|F<rsub|<text|weight>>>|<cell|=>|<cell|m*g<eq-number>>>|<row|<cell|m>|<cell|=>|<cell|6.14
    <text|kg><eq-number>>>|<row|<cell|g>|<cell|=>|<cell|9.82
    <frac|<text|m>|<text|s><rsup|2>><eq-number>>>|<row|<cell|P<rsub|<text|gauge>>>|<cell|=>|<cell|<frac|F<rsub|<text|weight>>|A><eq-number>>>|<row|<cell|A>|<cell|=>|<cell|\<pi\>\<cdot\><around*|(|<frac|d|2>|)><rsup|2><eq-number>>>|<row|<cell|d>|<cell|=>|<cell|1
    <text|cm><eq-number>>>|<row|<cell|P<rsub|<text|abs>>>|<cell|=>|<cell|P<rsub|<text|gauge>>+P<rsub|<text|atm>><eq-number><label|pressure
    absolute gauge equation>>>|<row|<cell|P<rsub|<text|atm>>>|<cell|=>|<cell|748<around*|(|<text|torr>|)><eq-number>>>|<row|<cell|1<around*|(|<text|torr>|)>>|<cell|=>|<cell|133.322
    <text|Pa><eq-number>>>>>
  </eqnarray*>

  The weight (force upon mass <math|m> by gravity acceleration <math|g>),
  <math|F<rsub|<text|weight>>>, is balanced by a pressure
  <math|P<rsub|<text|gauge>>> acting across cross-sectional area <math|A> of
  the cylindrical piston. The <em|absolute> pressure may be calculated by
  adding the gauge pressure <math|P<rsub|<text|gauge>>> to the barometric
  pressure (<math|748<around*|(|<text|torr>|)>>).

  <\eqnarray*>
    <tformat|<table|<row|<cell|P<rsub|<text|gauge>>>|<cell|=>|<cell|<frac|F<rsub|<text|weight>>|A><eq-number>>>|<row|<cell|P<rsub|<text|gauge>>>|<cell|=>|<cell|<frac|<around*|(|6.14
    <text|kg>|)>\<cdot\><around*|(|9.82 <frac|<text|m>|<text|s><rsup|2>>|)>|\<pi\>\<cdot\><around*|(|<frac|<around*|(|1
    cm|)>|2>|)><rsup|2>><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|6.14\<cdot\>9.82|\<pi\>\<cdot\><around*|(|<frac|1|2>|)><rsup|2>>|)>\<cdot\><around*|(|<text|kg>\<cdot\><frac|<text|m>|<text|s><rsup|2>>\<cdot\><around*|(|<frac|1|<text|cm>>|)><rsup|2>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|6.14\<cdot\>9.82|\<pi\>\<cdot\><around*|(|<frac|1|2>|)><rsup|2>>|)>\<cdot\><around*|(|<text|kg>\<cdot\><frac|<text|m>|<text|s><rsup|2>>\<cdot\><around*|(|<frac|1|<text|cm>>|)><rsup|2>*<around*|(|<frac|100
    <text|cm>|<text|m>>|)><rsup|2>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|6.14\<cdot\>9.82\<cdot\>100<rsup|2>|\<pi\>\<cdot\><around*|(|<frac|1|2>|)><rsup|2>>|)>*\<cdot\><around*|(|<text|kg>\<cdot\><frac|<text|m>|<text|s><rsup|2>>\<cdot\><frac|1|<text|m><rsup|2>>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|6.14\<cdot\>9.82\<cdot\>100<rsup|2>|\<pi\>\<cdot\><around*|(|<frac|1|2>|)><rsup|2>>|)>\<cdot\><around*|(|<frac|<text|N>|<text|m><rsup|2>>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|6.14\<cdot\>9.82\<cdot\>100<rsup|2>|\<pi\>\<cdot\><around*|(|<frac|1|2>|)><rsup|2>>|)>\<cdot\><text|Pa><eq-number>>>|<row|<cell|>|<cell|=>|<cell|767\<comma\>697.237
    <text|Pa><eq-number>>>|<row|<cell|P<rsub|<text|gauge>>>|<cell|=>|<cell|768\<comma\>000
    <text|Pa><eq-number>>>|<row|<cell|P<rsub|<text|gauge>>>|<cell|=>|<cell|768
    <text|kPa><eq-number>>>>>
  </eqnarray*>

  Using equation <reference|pressure absolute gauge equation>, the absolute
  pressure <math|P<rsub|<text|abs>>> can now be calculated:

  <\eqnarray*>
    <tformat|<table|<row|<cell|P<rsub|<text|abs>>>|<cell|=>|<cell|P<rsub|<text|gauge>>+P<rsub|<text|atm>><eq-number>>>|<row|<cell|>|<cell|=>|<cell|767\<comma\>697.237
    <text|Pa> + 748<around*|(|<text|torr>|)>\<cdot\><around*|(|<frac|133.322
    <text|Pa>|<around*|(|<text|torr>|)>>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|867\<comma\>422.093
    <text|Pa><eq-number>>>|<row|<cell|P<rsub|<text|abs>>>|<cell|=>|<cell|867\<comma\>000
    <text|Pa><eq-number>>>|<row|<cell|P<rsub|<text|abs>>>|<cell|=>|<cell|867
    <text|kPa><eq-number>>>>>
  </eqnarray*>

  <section|Example>

  <subsection|Prompt>

  At <math|27<rsup|\<circ\>><text|C>> the reading on a manometer filled with
  mercury is <math|60.5 <text|cm>>. The local acceleration of gravity is
  <math|9.784 <text|m>\<cdot\><text|s><rsup|-2>>. To what pressure does this
  height of mercury correspond?

  <subsection|Solution>

  Summarize givens:

  <\eqnarray*>
    <tformat|<table|<row|<cell|P>|<cell|=>|<cell|\<rho\><rsub|<text|Hg>>\<cdot\>*g\<cdot\>*h<eq-number>>>|<row|<cell|g>|<cell|=>|<cell|9.784
    <frac|<text|m>|<text|s><rsup|2>><eq-number>>>|<row|<cell|h>|<cell|=>|<cell|60.5
    <text|cm><eq-number>>>>>
  </eqnarray*>

  Look up density of liquid mercury at <math|27<rsup|\<circ\>><text|C>>. From
  <hlink|Perry's Chemical Engineer's Handbook 8th
  edition|https://www.worldcat.org/oclc/958502228>, page 2-97, Table 2-31
  Density of Mercury from <math|0> to <math|350<rsup|\<circ\>><text|C>>:

  <\equation>
    \<rho\><rsub|<text|Hg>>=13528.71 <frac|<text|kg>|<text|m><rsup|3>>
  </equation>

  Calculate <math|P>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|P>|<cell|=>|<cell|<around*|(|13\<comma\>528.71
    <frac|<text|kg>|<text|m><rsup|3>>|)>\<cdot\><around*|(|9.784
    <frac|<text|m>|<text|s><rsup|2>>|)>*<around*|(|60.5
    <text|cm>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|13\<comma\>528.71\<cdot\>9.784\<cdot\>60.5|)>\<cdot\><around*|(|<frac|<text|kg>|<text|m><rsup|3>>\<cdot\><frac|<text|m>|<text|s><rsup|2>>\<cdot\>cm\<cdot\><around*|(|<frac|<text|m>|100
    <text|cm>>|)>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|13\<comma\>528.71\<cdot\>9.784\<cdot\>60.5|100>|)>\<cdot\><around*|(|<frac|<text|kg>|<text|m><rsup|3>>\<cdot\><frac|<text|m>|<text|s><rsup|2>>\<cdot\><frac|<text|m>|1>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|13\<comma\>528.71\<cdot\>9.784\<cdot\>60.5|100>|)>\<cdot\><around*|(|<text|kg>\<cdot\><frac|<text|m>|<text|s><rsup|2>>\<cdot\><frac|1|<text|m><rsup|2>>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|13\<comma\>528.71\<cdot\>9.784\<cdot\>60.5|100>|)>\<cdot\><around*|(|<frac|<text|N>|<text|m><rsup|2>>|)>\<cdot\><around*|(|<frac|<text|Pa>|<around*|(|<frac|<text|N>|<text|m><rsup|2>>|)>>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|13\<comma\>528.71\<cdot\>9.784\<cdot\>60.5|100>|)>\<cdot\><text|Pa><eq-number>>>|<row|<cell|>|<cell|=>|<cell|80\<comma\>080.76368
    <text|Pa><eq-number>>>|<row|<cell|>|<cell|=>|<cell|80\<comma\>100
    <text|Pa><eq-number>>>|<row|<cell|>|<cell|=>|<cell|80.1
    <text|kPa><eq-number>>>>>
  </eqnarray*>

  <section|Example>

  <subsection|Prompt>

  An elevator with a mass of <math|2\<comma\>500 <text|kg>> rests at a level
  of <math|10 <text|m>> above the base of an elevator shaft. It is raised to
  <math|100 <text|m>> above the base of the shaft, where the cable holding it
  breaks. The elevator falls freely to the base of the shaft, where the cable
  holding it breaks. The elevator falls freely to the base of the shaft and
  strikes a strong spring. The spring is designed to bring the elevator to
  rest and, by means of a catch arrangement, to hold the elevator at the
  position of maximum spring compression. Assuming the entire process to be
  frictionless, and taking <math|g=9.8 <text|m>\<cdot\><text|s><rsup|-2>>,
  calculate:

  <\enumerate-alpha>
    <item>The potential energy of the elevator in its initial position
    relative to the base of the shaft.

    <item>The work done in raising the elevator.

    <item>The potential energy of the elevator in its highest position
    relative to the base of the shaft.

    <item>The velocity and kinetic energy of the elevator just before it
    strikes the spring.

    <item>The potential energy of the compressed spring.

    <item>The energy of the system consisting of the elevator and spring:

    <\enumerate-numeric>
      <item>at the start of the process

      <item>when the elevator reaches its maximum height

      <item>just before the elevator strikes the spring

      <item>after the elevator has come to rest.
    </enumerate-numeric>
  </enumerate-alpha>

  <subsection|Solution>

  <strong|a) The potential energy of the of the elevator in its initial
  position using as datum the base of the shaft>.

  <\equation>
    \<Delta\>*E<rsub|P<text|,init>>=m*z<rsub|1>*g-m*z<rsub|0>*g<label|4.2
    elevator initial potential energy>
  </equation>

  where:

  <\eqnarray*>
    <tformat|<table|<row|<cell|m>|<cell|:>|<cell|<text|mass of the
    elevator>>>|<row|<cell|z<rsub|0>>|<cell|:>|<cell|<text|elevation of the
    elevator at the base of the shaft>>>|<row|<cell|z<rsub|1>>|<cell|:>|<cell|<text|the
    initial elevation of the elevator <math|10 <text|m>> above the base of
    the shaft>>>|<row|<cell|g>|<cell|:>|<cell|<text|acceleration due to
    gravity>>>|<row|<cell|\<Delta\>*E<rsub|P<text|,init>>>|<cell|:>|<cell|<text|difference
    in potential energy between <math|z<rsub|1>> and <math|z<rsub|0>>
    positions>>>>>
  </eqnarray*>

  Givens are:

  <\eqnarray*>
    <tformat|<table|<row|<cell|m>|<cell|=>|<cell|2500
    <text|kg><eq-number>>>|<row|<cell|z<rsub|0>>|<cell|=>|<cell|0
    <text|m><eq-number>>>|<row|<cell|z<rsub|1>>|<cell|=>|<cell|10
    <text|m><eq-number>>>|<row|<cell|g>|<cell|=>|<cell|9.8
    <frac|<text|m>|<text|s><rsup|2>><eq-number>>>>>
  </eqnarray*>

  Calculate potential energy in elevator at <math|z<rsub|1>=10 <text|m>> with
  respect to <math|z<rsub|0>=0 <text|m>>.

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Delta\>*E<rsub|P<text|,init>>>|<cell|=>|<cell|E<rsub|P,1>-E<rsub|P,0><eq-number>>>|<row|<cell|>|<cell|=>|<cell|m*z<rsub|1>*g-m*z<rsub|0>*g<eq-number>>>|<row|<cell|>|<cell|=>|<cell|m\<cdot\>g*\<cdot\><around*|(|z<rsub|1>-z<rsub|0>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|2500
    <text|kg>|)>\<cdot\><around*|(|9.8 <frac|<text|m>|<text|s><rsup|2>>|)>*<around*|(|10
    <text|m> - 0 <text|m>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|2500\<cdot\>9.8\<cdot\>10|)>\<cdot\><around*|(|<text|kg>\<cdot\><frac|<text|m>|<text|s><rsup|2>>\<cdot\><text|m>|)>*<around*|(|<frac|<text|J>|<text|kg>\<cdot\><frac|<text|m><rsup|2>|<text|s><rsup|2>>>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|245\<comma\>000
    <text|J><eq-number>>>>>
  </eqnarray*>

  The potential energy of the elevator in its initial position relative to
  the base of the shaft is <math|245\<comma\>000 <text|J>>.

  <strong|b) The work done in raising the elevator.>

  The elevator is raised from <math|z<rsub|>=z<rsub|1>=10> to
  <math|z=z<rsub|2>=100>. The work done is the same as the potential energy
  change of going from the <math|z<rsub|1>=10> state to the
  <math|z<rsub|2>=100> state. The equation for this potential energy change
  resembles equation <reference|4.2 elevator initial potential energy> but
  with new indices for <math|z>.

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Delta\>*E<rsub|P,1\<rightarrow\>2>>|<cell|=>|<cell|E<rsub|P,2>-E<rsub|P,1><eq-number>>>|<row|<cell|>|<cell|=>|<cell|m*z<rsub|2>*g-m*z<rsub|1>*g>>|<row|<cell|>|<cell|=>|<cell|m\<cdot\>g\<cdot\><around*|(|z<rsub|2>-z<rsub|1>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|2500
    <text|kg>|)>\<cdot\><around*|(|9.8 <frac|<text|m>|<text|s><rsup|2>>|)>*<around*|(|100
    <text|m> - 10 <text|m>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|2500\<cdot\>9.8\<cdot\>90|)>\<cdot\><around*|(|<text|kg>\<cdot\><frac|<text|m>|<text|s><rsup|2>>\<cdot\><text|m>|)>*<around*|(|<frac|<text|J>|<text|kg>\<cdot\><frac|<text|m><rsup|2>|<text|s><rsup|2>>>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|2\<comma\>205\<comma\>000
    <text|J><eq-number>>>>>
  </eqnarray*>

  <strong|c) The potential energy of the elevator in its highest position
  relative to the base of the shaft.>

  The potential energy difference between the highest state
  (<math|z=z<rsub|2>=100 <text|m>>) and the base of the shaft state
  (<math|z=z<rsub|0>=0 <text|m>>) may be calculated similarly to equation
  <reference|4.2 elevator initial potential energy> but, again, with new
  indices for <math|z>.

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Delta\>*E<rsub|P,0\<rightarrow\>2>>|<cell|=>|<cell|E<rsub|P,2>-E<rsub|P,0><eq-number>>>|<row|<cell|>|<cell|=>|<cell|m*z<rsub|2>*g-m*z<rsub|0>*g<eq-number>>>|<row|<cell|>|<cell|=>|<cell|m\<cdot\>g\<cdot\><around*|(|z<rsub|2>-z<rsub|0>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|2500
    <text|kg>|)>\<cdot\><around*|(|9.8 <frac|<text|m>|<text|s><rsup|2>>|)>*<around*|(|100
    <text|m> - 0 <text|m>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|2500\<cdot\>9.8\<cdot\>100|)>\<cdot\><around*|(|<text|kg>\<cdot\><frac|<text|m>|<text|s><rsup|2>>\<cdot\><text|m>|)>*<around*|(|<frac|<text|J>|<text|kg>\<cdot\><frac|<text|m><rsup|2>|<text|s><rsup|2>>>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|2\<comma\>450,000
    <text|J><eq-number>>>>>
  </eqnarray*>

  <strong|d) The velocity and kinetic energy of the elevator just before it
  strikes the spring>

  At the point when the elevator strikes the spring, it has fallen to the
  bottom of the shaft and therefore reached elevation
  <math|z=z<rsub|3>=z<rsub|0>=0 <text|m>>. Because
  <math|z<rsub|3>=z<rsub|0>>, the potential energy of the elevator is zero
  (since elevation <math|z<rsub|0>> was used as the datum for potential
  energy calculations). Because energy must be conserved (and because we are
  assuming that the process is frictionless), this also means that all
  potential energy the elevator had in state <math|z=z<rsub|2>=100 <text|m>>
  has been converted into kinetic energy by the time the elevator has fallen
  to the bottom fo the shaft (state <math|z=z<rsub|3>>). This can be seen by
  writing the equation for the conservation of energy for kinetic and
  potential energy as applied to this example in going from state
  <math|z=z<rsub|2>=100 <text|m>> to <math|z=z<rsub|3>=0 <text|m>>

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Delta\>*E<rsub|K,2\<rightarrow\>3>+\<Delta\>*E<rsub|P,2\<rightarrow\>3>>|<cell|=>|<cell|0<eq-number>>>|<row|<cell|E<rsub|K,3>-E<rsub|K,2>+E<rsub|P,3>-E<rsub|P,2>>|<cell|=>|<cell|0<eq-number><label|elevator
    energy conservation>>>>>
  </eqnarray*>

  <math|E<rsub|K,2>=0 <text|J>> because the elevator was not moving when the
  cable broke.

  <math|E<rsub|P,3>=0 <text|J>> because the elevator reaches
  <math|z=z<rsub|3>=0 <text|m>> just before it strikes the spring.

  Therefore, equation <reference|elevator energy conservation> may be
  simplified to:

  <\eqnarray*>
    <tformat|<table|<row|<cell|E<rsub|K,3>-E<rsub|P,2>>|<cell|=>|<cell|0<eq-number>>>|<row|<cell|E<rsub|K,3>>|<cell|=>|<cell|E<rsub|P,2><eq-number>>>|<row|<cell|E<rsub|K,3>>|<cell|=>|<cell|m*z<rsub|2>*g<eq-number>>>|<row|<cell|E<rsub|K,3>>|<cell|=>|<cell|2\<comma\>450\<comma\>000
    <text|J><eq-number>>>>>
  </eqnarray*>

  This means that the kinetic energy just before the elevator strikes the
  spring at the bottom of the shaft is equal to the potential energy the
  elevator had just before the cable broke when it was at rest at
  <math|z=z<rsub|2>=100 <text|m>>.

  Since <math|E<rsub|P,2>> is known, we can use the equation for kinetic
  energy <math|E<rsub|K,3>> to solve for velocity <math|u<rsub|3>>.

  <\eqnarray*>
    <tformat|<table|<row|<cell|E<rsub|K,3>>|<cell|=>|<cell|<frac|1|2>*\<cdot\>m\<cdot\>u<rsub|3><rsup|2><eq-number>>>|<row|<cell|<sqrt|<frac|2\<cdot\>E<rsub|K,3>|m>>>|<cell|=>|<cell|u<rsub|3><eq-number>>>|<row|<cell|u<rsub|3>>|<cell|=>|<cell|<sqrt|<frac|2\<cdot\><around*|(|2\<comma\>450\<comma\>000
    <text|J>|)>|2\<comma\>500 <text|kg>>><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<sqrt|<frac|2\<cdot\><around*|(|2\<comma\>450\<comma\>000
    <text|kg>*<frac|<text|m><rsup|2>|<text|s><rsup|2>>|)>|2\<comma\>500
    <text|kg>>><eq-number>>>|<row|<cell|>|<cell|=>|<cell|<sqrt|<frac|2\<cdot\>2\<comma\>450\<comma\>000|2\<comma\>500>>\<cdot\><around*|(|<frac|<text|m>|<text|s>>|)><eq-number>>>|<row|<cell|u<rsub|3>>|<cell|=>|<cell|44.27
    <frac|<text|m>|<text|s>><eq-number>>>>>
  </eqnarray*>

  The velocity of the elevator just before it strikes the spring at the
  bottom of the shaft is <math|44.27 <frac|<text|m>|<text|s>>>.

  <strong|e) The potential energy of the compressed spring>

  Due to the latching mechanism described in the prompt and the fact that all
  mechanical processes are frictionless without energy leaking into other
  forms (ex: heat), the potential energy of the compressed spring must be
  equal to the kinetic energy of the elevator at the moment just before
  striking the spring. This also equals the potential energy fo the elevator
  at the moment just before the cable broke as it rested at
  <math|z=z<rsub|2>=100 <text|m>>. This potential energy equals
  <math|2\<comma\>450\<comma\>000 <text|J>>.

  <\strong>
    f.1) The energy of the system consisting of the elevator and spring at
    the start of the process
  </strong>

  \PAn elevator with mass of <math|2\<comma\>500 <text|kg>> rests at a level
  <math|10 <text|m>> above the base of an elevator shaft.\Q

  No potential or kinetic energy is stored in the spring.

  The elevator has zero kinetic energy because it has zero velocity (at
  rest).

  The elevator at <math|z<rsub|1>=10 <text|m>> has
  <math|E<rsub|P,1>=m*z<rsub|1>*g=<around*|(|2\<comma\>500
  <text|kg>|)>\<cdot\><around*|(|10 <text|m>|)>\<cdot\><around*|(|9.8
  <frac|<text|m>|<text|s><rsup|2>>|)>=245\<comma\>000 <text|J>> of potential
  energy with respect to its base state of <math|z=0 <text|m>>.

  <strong|f.2) The energy of the system consisting of the elevator and spring
  when the elevator reaches its maximum height.>

  \PIt is raised to <math|100 <text|m>> above the base of the shaft, where
  the cable holding it breaks.\Q

  No potential or kinetic energy is stored in the spring.

  The elevator has zero kinetic energy because this is the simplest
  interpretation of the prompt (technically the elevator could be rising when
  the cable fails at <math|z=z<rsub|2>=100 <text|m>> but no information about
  its velocity is given at its maximum height. Without further information,
  the simplest interpretation is to assume that the elevator's velocity at
  its maximum height is zero.

  The elevator at its maximum height of <math|z=z<rsub|2>=100 <text|m>> has
  <math|E<rsub|P,2>=m*z<rsub|2>*g=<around*|(|2\<comma\>500
  <text|kg>|)>\<cdot\><around*|(|100 <text|m>|)>\<cdot\><around*|(|9.8
  <frac|<text|m>|<text|s><rsup|2>>|)>=2\<comma\>450\<comma\>000 <text|J>> of
  potential energy with respect to its base state of <math|z=0 <text|m>>.

  <strong|f.3) The energy of the system consisting of the elevator and spring
  just before the elevator strikes the spring>

  No potential or kinetic energy is stored in the spring since the elevator
  has not yet made contact with the spring.

  The elevator just before hitting the spring has zero potential energy
  because it has fallen to its potential energy datum elevation of <math|z=0
  <text|m>>.

  The elevator just before hitting the spring has a kinetic energy that
  equals in magnitude the potential energy it had accumulated from being
  raised to <math|z=100 <text|m>> just before the cable snapped. This amount
  of energy is <math|2\<comma\>450\<comma\>000 <text|J>>.

  <strong|f.4) The energy of the system consisting of the elevator and spring
  after the elevator has come to rest>

  The spring contains <math|2\<comma\>450\<comma\>000 <text|J>> of potential
  energy stored in the elastic deformation of its structure. This is because
  the prompt specified that the spring was designed to \Pbring the elevator
  to rest\Q and \Phold the elevator at the position of maximum spring
  compression\Q \Pby means of a catch arrangement\Q. In order to bring the
  elevator to rest at the bottom of the shaft, both its kinetic and potential
  energy must be transferred out of the elevator. The spring performs this
  function. The catch mechanism ensures that the spring does not transfer the
  energy it absorbed back into the elevator.

  The elevator has zero kinetic energy and zero potential energy.

  In summary, because no mechanical energy is lost to friction, the total
  energy (in either potential or kinetic forms) in the elevator/spring system
  after the cable breaks remains at a constant <math|2\<comma\>450,000
  <text|J>>.

  \;
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|4.2 elevator initial potential energy|<tuple|53|4>>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-10|<tuple|4|4>>
    <associate|auto-11|<tuple|4.1|4>>
    <associate|auto-12|<tuple|4.2|4>>
    <associate|auto-2|<tuple|1.1|1>>
    <associate|auto-3|<tuple|1.2|1>>
    <associate|auto-4|<tuple|2|2>>
    <associate|auto-5|<tuple|2.1|2>>
    <associate|auto-6|<tuple|2.2|2>>
    <associate|auto-7|<tuple|3|3>>
    <associate|auto-8|<tuple|3.1|3>>
    <associate|auto-9|<tuple|3.2|3>>
    <associate|elevator energy conservation|<tuple|76|5>>
    <associate|pressure absolute gauge equation|<tuple|22|2>>
    <associate|weight equation earth|<tuple|2|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Example>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Prompt
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>Solution
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Example>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4><vspace|0.5fn>

      <with|par-left|<quote|1tab>|2.1<space|2spc>Prompt
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|2.2<space|2spc>Solution
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Example>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7><vspace|0.5fn>

      <with|par-left|<quote|1tab>|3.1<space|2spc>Prompt
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|1tab>|3.2<space|2spc>Solution
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>Example>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10><vspace|0.5fn>

      <with|par-left|<quote|1tab>|4.1<space|2spc>Prompt
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|1tab>|4.2<space|2spc>Solution
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>
    </associate>
  </collection>
</auxiliary>